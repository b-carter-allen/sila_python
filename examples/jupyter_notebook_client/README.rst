jupyter-notebook SiLA client
===============================

This is a demo of a jupyter-notebook SiLA client.


Installation of the jupyter notebook in a virtual environment
---------------------------------------------------------------

Before you can run the notebook two commands need to be issued once:

In your activated virtual python environment, install

.. code-block:: console

   pip install ipython
   ipython kernel install --user --name=[name of your venv]


Running the notebook
----------------------

The jupyter nootebook can be started with

.. code-block:: console

   jupyter-notebook

When the notebook has started, select the
[name of you venv] as Kernel in upper right corner of the webpage and start coding...
