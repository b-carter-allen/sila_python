This examples includes feature definitions that represent all sorts of existing data types. It can not be executed, but a build script (`build.py`) is included which allows to use the SiLA library functionality to build the corresponding `.proto` files as well as the grpc and protobuf python implementations.

The source files for this example have been taken from the [sila_java](https://gitlab.com/SiLA2/sila_java/tree/master/servers/test_server/src/main/resources/features) repository.
