
import logging

import os

from sila2lib.fdl_parser.fdl_parser import FDLParser
from sila2lib.proto_builder.proto_builder import ProtoBuilder
from sila2lib.proto_builder.proto_compiler import compile_proto

# --------------------------------------
# Input configuration
# --------------------------------------

# logging details
logging.basicConfig(format='%(levelname)s | %(module)s.%(funcName)s: %(message)s', level=logging.INFO)

# features to convert
features = ['ObservableCommandTest', 'ObservablePropertyTest',
            'ParameterConstraintTest',
            'UnobservableCommandTest', 'UnobservablePropertyTest']

# --------------------------------------
# Create protobuf files
# --------------------------------------

# create output directory
os.makedirs('protobuf', exist_ok=True)

for feature in features:
    logging.info('Building .proto files for feature {feature}'.format(feature=feature))

    # create input paths
    input_file = os.path.join('.', 'features', (feature + '.sila.xml'))
    output_path = os.path.join('.', 'protobuf')

    #   Actual conversion
    fdl_parser = FDLParser(fdl_filename=input_file)
    proto_builder = ProtoBuilder(fdl_parser=fdl_parser)
    proto_file = proto_builder.write_proto(proto_dir=output_path)
    logging.info('Written result to {proto_file}'.format(proto_file=proto_file))

# --------------------------------------
# Create gRPC proto stubs
# --------------------------------------

os.makedirs('stubs', exist_ok=True)

for feature in features:
    logging.info('Building stubs for feature'.format(feature=feature))

    # create paths
    input_file = feature + '.proto'
    input_path = os.path.join('.', 'protobuf')
    output_path = os.path.join('.', 'stubs')

    if compile_proto(proto_file=input_file, source_dir=input_path, target_dir=output_path, auto_include_library=True):
        logging.info('File compiled successfully.')
    else:
        logging.warning('Error while compiling proto file "{proto_file}"'.format(
            proto_file=os.path.join(input_path, input_file)
        ))