Introduction into python_sila
================================

*SiLA_python* is a python implementation of the universal and royalty free **[SiLA 2](https://sila-standard.org)** laboratory automation standard. It provides convenient libraries, a codegenerator and a collection of examples and implementations to support a fast integration of SiLA 2 into your own lab automation projects and to illustrate how SiLA 2 could be implemented.

This is currently the fastest route to a SiLA2 experience. With the
SiLA2 python installer - *sila2install.py* (s.
[Quickstart](#quickstart)) - you will be able to run a first SiLA2
HelloWorld Service within *5 minutes* !

*Please note that **Python &gt;3.6** is mandatory to run the code in this
repository.*
