SiLA2 server / client development workflow
============================================

Conceptualisation
------------------

To start with the development, please first sit down with a pencil and a piece
of paper and think about the desired functionality,
design and architecture of your future SiLA 2 application.

Break everything down into functional units. These can be directly transformed into
**SiLA Features**.
During this break-down process, also consider, if you can formulate the feature
in a more general way that one could re-use it later for different applications.

SiLA Feature Design
---------------------

SiLA feature design is an art: One needs to predict, what will be needed in the future
and how the total application shall work.
Think, about a good behaviour, what commands you are planning to send to the server,
what answers you are expecting to recieve. These will become **SiLA Commands**.
For server-inherent properties you can formulate **SiLA Properties**.
Possible errors, which are already predictable, note as **DefinedErrors**.

Once the disgn is clear, open an XML editor (like, e.g., *PyCharm*, *IntelliJ*, ...)
that has an inherent XML schema validation, load the SiLAFeature Scheme into the
validator and start writing the *Feature* in the *SiLA Feature Devinition XML language FDL*.

Please be short, but complete in your descriptions. It should be clear, what each
element of the feature does without reading further documentation.
Please describe all the expected, states the server might have.

Feel also free to check the SiLA_base repository for existing features
you can incorporate into your own current project - please cooperate with others to
improve features and commit them to the sila_base repository.

Finally please validate your feature against the current *SiLAFeature.sila.xml* Scheme.

Translation of the FDL-files into real python code
----------------------------------------------------

Once your are ready with all the features, please collect all desired features
into one directory and add a **service_description.json** file.

This service description file (in JSON format) should contain the following keys:

.. code-block:: JSON

    {
      "service_name" : "HelloSiLA2",
      "service_description": "This is Hello World SiLA2 test service",
      "authors"      : "benjamin lear",
      "creation_date" : "2019-11-06",
      "version" : "0.0.1",
      "vendor_url" : "lara.uni-greifswald.de",
      "SiLA_standard_feature_list" : ["SiLAService"],
      "SiLA_feature_list" : ["GreetingProvider"],
      "communication_port" : "50051",
      "IP_address" : "127.0.0.1",
      "hostname" : "localhost"
    }

Where:
  **"service_name"** : is the name of the server, but also the generated directory,
  **"service_description"**: a short description of the service,
  **"authors"**      : your name,
  **"creation_date"** : the date, when you created the service,
  **"version"** : a version string, preferrable in SemanticVersioning,
  **"vendor_url"** : your URL,
  **"SiLA_standard_feature_list"** : list of all SiLA standard features, found in sila_base org.silastandard,
  **"SiLA_feature_list"** : list of all your features in this directory, you want to incorporate,
  **"communication_port"** : a communcation port number,
  **"IP_address"** : the IP address of the SiLA server,
  **"hostname"** : a hostname of the SiLA 2 server

When your files are ready, change to the directory above your collection and start the **silacodegenerator**:

.. code-block:: console

   silacodegenerator --build my_new_sila_project

The **silacodegenerator** will first translate your FDL files into **protobuf** files.
These files are then further translated into the stubs required for the gRPC communication.
Finally the **silacodegenerator** will generate functional python prototype code for
the server and the client.
It can be already started, by typing (of course with a default behaviour) :

.. code-block:: console

   python3 my_new_sila_server.py

   # and in another terminal

   python3 my_new_sila_server.py

  # (mind that python >= 3.6 is required)

Our *codegenerator* created code for  *simulation* and *real mode* of the server.

To finish the server and client development, please add the desired functionality
to the *real mode module* (e.g. serial interface connections and serial commands).

Tip: The sila_com_library contains a collection of coummunication modules to make
     hardware communcation even simpler.

**Happy coding !**
