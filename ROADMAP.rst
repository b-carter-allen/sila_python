sila\_python Roadmap
====================

0.2
---

-  dynamic properties
-  observable commands
-  error handling
-  zeroconf auto ip and port
-  device detection by name
-  interoperability with JAVA and C#
-  advanced registering (dev. image)

0.3
---

-  full data type support
-  reflection (?)
-  late binding concepts

0.4
---

-  more std features
-  logging / audit
-  device locking

0.5
---

-  encryption
-  authentification concepts (preparation)

0.8
---

-  large binary
-  constraints
-  complex data types

1.0
---

-  most important concepts of 2019/Q3 Part A and B implemented
