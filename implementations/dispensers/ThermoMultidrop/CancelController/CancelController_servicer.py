"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Cancel Controller*

:details: CancelController:
    This feature offers commands to cancel/terminate Commands. Cancellation is the act of stopping the running Command
    execution(s),
    irrevocably. The SiLA Server SHOULD be able to be in a state where any further
    commands can be issued after a cancellation.
           
:file:    CancelController_servicer.py
:authors: unitelabs, mark doerr

:date: (creation)          2019-11-10T11:45:26.089980
:date: (last modification) 2019-11-10T11:45:26.089980

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.1"

# import general packages
import logging
import grpc

# meta packages
from typing import Union

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
from sila2lib.error_handling.server_err import SiLAError

# import gRPC modules for this feature
from .gRPC import CancelController_pb2 as CancelController_pb2
from .gRPC import CancelController_pb2_grpc as CancelController_pb2_grpc

# import simulation and real implementation
from .CancelController_simulation import CancelControllerSimulation
from .CancelController_real import CancelControllerReal


class CancelController(CancelController_pb2_grpc.CancelControllerServicer):
    """
    SiLA2 service for Thermo Multidrop 8 channel dispenser
    """
    implementation: Union[CancelControllerSimulation, CancelControllerReal]
    simulation_mode: bool

    def __init__(self, simulation_mode: bool = True):
        """
        Class initialiser.

        :param simulation_mode: Sets whether at initialisation the simulation mode is active or the real mode.
        """

        self.simulation_mode = simulation_mode
        if simulation_mode:
            self._inject_implementation(CancelControllerSimulation())
        else:
            self._inject_implementation(CancelControllerReal())

    def _inject_implementation(self,
                               implementation: Union[CancelControllerSimulation,
                                                     CancelControllerReal]
                               ) -> bool:
        """
        Dependency injection of the implementation used.
            Allows to set the class used for simulation/real mode.

        :param implementation: A valid implementation of the ThermoMultidropServicer.
        """

        self.implementation = implementation
        return True

    def switch_to_simulation_mode(self):
        """Method that will automatically be called by the server when the simulation mode is requested."""
        self.simulation_mode = True
        self._inject_implementation(CancelControllerSimulation())

    def switch_to_real_mode(self):
        """Method that will automatically be called by the server when the real mode is requested."""
        self.simulation_mode = False
        self._inject_implementation(CancelControllerReal())

    def CancelCommand(self, request, context: grpc.ServicerContext) \
            -> CancelController_pb2.CancelCommand_Responses:
        """
        Executes the unobservable command "Cancel Command"
            Cancel a specified currently running Observable Command or cancel all currently running Commands (Observable and
            Unobservable).
            For any canceled Observable Command the SiLA Server MUST update the Command Execution Status to "Command Finished
            with Error".
            The SiLA Server MUST throw a descriptive error message indicating cancellation as the reason for the Command
            execution not being able to finish successfully for any canceled Command.
    
        :param request: gRPC request containing the parameters passed:
            request.CommandExecutionUUID (Command Execution UUID): The Command Execution UUID according to the SiLA Standard.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "CancelCommand called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.CancelCommand(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def CancelAll(self, request, context: grpc.ServicerContext) \
            -> CancelController_pb2.CancelAll_Responses:
        """
        Executes the unobservable command "Cancel All"
            Cancels all currently running Observable and Unobservable Commands running on this SiLA Server.
            The SiLA Server MUST throw an Execution Error indicating 'cancellation' as the reason for the
            Command not being able to finish successfully.
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "CancelAll called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.CancelAll(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)

    
